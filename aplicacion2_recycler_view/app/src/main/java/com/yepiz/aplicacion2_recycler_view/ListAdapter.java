package com.yepiz.aplicacion2_recycler_view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder>{
    private ListData[] listData;

    public ListAdapter(ListData[] listData){
        this.listData=listData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,int viewType){
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View telefonosItem=layoutInflater.inflate(R.layout.telefono_item,parent,false);
        ViewHolder viewHolder=new ViewHolder(telefonosItem);
        return  viewHolder;
    }
    @Override
    public  int getItemCount(){
        return listData.length;
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position){
        final ListData nlistData = listData[position];
        viewHolder.textView2.setText(listData[position].getNombre());
        viewHolder.textView.setText(listData[position].getMarca());
        viewHolder.imageView.setImageResource(listData[position].getImagen());
        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),"Se seleccionó el elemento: "+nlistData.getNombre(),Toast.LENGTH_LONG).show();
            }
        });
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView imageView;
        public TextView textView, textView2;
        public LinearLayout linearLayout;

        public ViewHolder(View view){
            super(view);
            this.imageView=(ImageView) view.findViewById(R.id.telefonos_imagen);
            this.textView=(TextView) view.findViewById(R.id.telefonos_nombre);
            this.textView2=(TextView) view.findViewById(R.id.telefonos_categoria);
            this.linearLayout=(LinearLayout) view.findViewById(R.id.layout_telefonos);
        }
    }

}

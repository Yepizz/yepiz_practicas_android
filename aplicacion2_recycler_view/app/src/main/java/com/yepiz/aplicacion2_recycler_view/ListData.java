package com.yepiz.aplicacion2_recycler_view;

public class ListData {
    private String nombre;
    private String modelo;
    private  String marca;
    private  int imagen;

public ListData(String nombre, String modelo, String marca ,int imagen){
    this.nombre = nombre;
    this.modelo = modelo;
    this.marca = marca;
    this.imagen = imagen;
}

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getModelo() { return modelo; }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }
}

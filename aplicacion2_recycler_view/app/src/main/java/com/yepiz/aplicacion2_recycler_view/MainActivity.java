package com.yepiz.aplicacion2_recycler_view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListData[] listData = new ListData[]{
                new ListData("Iphone 7","A1416","Apple",R.drawable.iphone7),
                new ListData("Iphone 8","A1245","Apple",R.drawable.iphone8),
                new ListData("Iphone 10","A9856","Apple",R.drawable.iphone10),
                new ListData("Iphone 11","A1568","Apple",R.drawable.iphone11),
                new ListData("Iphone 12","A5478","Apple",R.drawable.iphone12),
                new ListData("Iphone 7","A1416","Apple",R.drawable.iphone7),
                new ListData("Iphone 8","A1245","Apple",R.drawable.iphone8),
                new ListData("Iphone 10","A9856","Apple",R.drawable.iphone10),
                new ListData("Iphone 11","A1568","Apple",R.drawable.iphone11),
                new ListData("Iphone 12","A5478","Apple",R.drawable.iphone12),
                new ListData("Iphone 7","A1416","Apple",R.drawable.iphone7),
                new ListData("Iphone 8","A1245","Apple",R.drawable.iphone8),
                new ListData("Iphone 10","A9856","Apple",R.drawable.iphone10),
                new ListData("Iphone 11","A1568","Apple",R.drawable.iphone11),
                new ListData("Iphone 12","A5478","Apple",R.drawable.iphone12),
                new ListData("Iphone 7","A1416","Apple",R.drawable.iphone7),
                new ListData("Iphone 8","A1245","Apple",R.drawable.iphone8),
                new ListData("Iphone 10","A9856","Apple",R.drawable.iphone10),
                new ListData("Iphone 11","A1568","Apple",R.drawable.iphone11),
                new ListData("Iphone 12","A5478","Apple",R.drawable.iphone12),
                new ListData("Iphone 7","A1416","Apple",R.drawable.iphone7),
                new ListData("Iphone 8","A1245","Apple",R.drawable.iphone8),
                new ListData("Iphone 10","A9856","Apple",R.drawable.iphone10),
                new ListData("Iphone 11","A1568","Apple",R.drawable.iphone11),
                new ListData("Iphone 12","A5478","Apple",R.drawable.iphone12),
                new ListData("Iphone 7","A1416","Apple",R.drawable.iphone7),
                new ListData("Iphone 8","A1245","Apple",R.drawable.iphone8),
                new ListData("Iphone 10","A9856","Apple",R.drawable.iphone10),
                new ListData("Iphone 11","A1568","Apple",R.drawable.iphone11),
                new ListData("Iphone 12","A5478","Apple",R.drawable.iphone12),
                new ListData("Iphone 7","A1416","Apple",R.drawable.iphone7),
                new ListData("Iphone 8","A1245","Apple",R.drawable.iphone8),
                new ListData("Iphone 10","A9856","Apple",R.drawable.iphone10),
                new ListData("Iphone 11","A1568","Apple",R.drawable.iphone11),
                new ListData("Iphone 12","A5478","Apple",R.drawable.iphone12),
                new ListData("Iphone 7","A1416","Apple",R.drawable.iphone7),
                new ListData("Iphone 8","A1245","Apple",R.drawable.iphone8),
                new ListData("Iphone 10","A9856","Apple",R.drawable.iphone10),
                new ListData("Iphone 11","A1568","Apple",R.drawable.iphone11),
                new ListData("Iphone 12","A5478","Apple",R.drawable.iphone12),
                new ListData("Iphone 7","A1416","Apple",R.drawable.iphone7),
                new ListData("Iphone 8","A1245","Apple",R.drawable.iphone8),
                new ListData("Iphone 10","A9856","Apple",R.drawable.iphone10),
                new ListData("Iphone 11","A1568","Apple",R.drawable.iphone11),
                new ListData("Iphone 12","A5478","Apple",R.drawable.iphone12),
                new ListData("Iphone 7","A1416","Apple",R.drawable.iphone7),
                new ListData("Iphone 8","A1245","Apple",R.drawable.iphone8),
                new ListData("Iphone 10","A9856","Apple",R.drawable.iphone10),
                new ListData("Iphone 11","A1568","Apple",R.drawable.iphone11),
                new ListData("Iphone 12","A5478","Apple",R.drawable.iphone12),
                new ListData("Iphone 7","A1416","Apple",R.drawable.iphone7),
                new ListData("Iphone 8","A1245","Apple",R.drawable.iphone8),
                new ListData("Iphone 10","A9856","Apple",R.drawable.iphone10),
                new ListData("Iphone 11","A1568","Apple",R.drawable.iphone11),
                new ListData("Iphone 12","A5478","Apple",R.drawable.iphone12)
        };

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recView);
        ListAdapter listAdapter = new ListAdapter(listData);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(listAdapter);
    }
}